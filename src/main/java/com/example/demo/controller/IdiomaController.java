package com.example.demo.controller;

import java.util.ArrayList;

public class IdiomaController {

    public void crearLanguaje(String idioma) {
        System.out.println("el idioma" + idioma + "ha sido creado");
    }

    public void actualizarLanguaje(int id, String idioma) {
        System.out.println("El idioma" + idioma + "con el id " + id + "ha sido creado");
    }

    public void deleteLanguaje(int id, String idioma) {
        System.out.println("El idioma" + idioma + "con el id " + id + "ha sido eliminado");
    }
    
    public void insertarIdioma() {
    	System.out.println("se inserto");
    }
    
    public void listarIdioma() {
    	
    	ArrayList<String> idioma = new ArrayList<String>();
    	idioma.add("ingles");
    	idioma.add("Frances");
     
    }
}
